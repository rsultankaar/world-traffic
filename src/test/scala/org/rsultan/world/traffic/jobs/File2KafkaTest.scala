package org.rsultan.world.traffic.jobs

import java.io.File

import org.rsultan.world.traffic.config.EmbeddedKafkaJunit.{kafkaRule, PORT}
import org.rsultan.world.traffic.constant.TestConstants.{routes, airports}
import org.junit
import org.junit.runner.RunWith
import org.junit.runners.BlockJUnit4ClassRunner
import org.rsultan.world.traffic.config.EmbeddedKafkaJunit
import org.rsultan.world.traffic.serde.{RouteSerializer, StationSerializer}
import org.rsultan.world.traffic.utils.KafkaUtils

@RunWith(classOf[BlockJUnit4ClassRunner])
class File2KafkaTest extends EmbeddedKafkaJunit {

  private val stationFile = "stations.txt"
  private val stationFileEmpty = "stations-empty.txt"
  private val routeFile = "routes.txt"
  private val routeFileEmpty = "routes-empty.txt"

  private val goodFiles = Seq("--airports", getAbsolutePath(stationFile), "--routes", getAbsolutePath(routeFile))
  private val emptyStations = Seq("--airports", getAbsolutePath(stationFileEmpty), "--routes", getAbsolutePath(routeFile))
  private val emptyRoutes = Seq("--airports", getAbsolutePath(stationFile), "--routes", getAbsolutePath(routeFileEmpty))
  private val kafkaConfig = Seq("--airport-topic", "stations", "--routes-topic", "routes", "--kafka-brokers", "localhost:" + PORT)

  @junit.Test def `must_write_files_into_kafka` {
    Airport2Kafka(goodFiles ++ kafkaConfig).run()
    KafkaUtils.testTopic(kafkaRule.helper(), "stations", 3, new StationSerializer, airports.map(_.toStation))
    KafkaUtils.testTopic(kafkaRule.helper(), "routes", 6, new RouteSerializer, routes.map(_.toStationRoute))
  }

  @junit.Test def `must_write_stations_into_kafka` {
    Airport2Kafka(emptyRoutes ++ kafkaConfig).run()
    KafkaUtils.testTopic(kafkaRule.helper(), "stations", 3, new StationSerializer, airports.map(_.toStation))
    KafkaUtils.testTopic(kafkaRule.helper(), "routes", 0, new RouteSerializer, Seq())
  }

  @junit.Test def `must_write_nothing_into_kafka` {
    Airport2Kafka(emptyStations ++ kafkaConfig).run()
    KafkaUtils.testTopic(kafkaRule.helper(), "stations", 0, new StationSerializer, Seq())
    KafkaUtils.testTopic(kafkaRule.helper(), "routes", 0, new RouteSerializer, Seq())
  }


  private def getAbsolutePath(fileName: String): String = {
    new File(getClass.getClassLoader.getResource(fileName).getFile).getAbsolutePath
  }
}