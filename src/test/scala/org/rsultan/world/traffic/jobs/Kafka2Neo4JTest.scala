package org.rsultan.world.traffic.jobs

import org.fest.assertions.api.Assertions.assertThat
import org.junit.runner.RunWith
import org.junit.runners.BlockJUnit4ClassRunner
import org.junit.{After, Before, Test}
import org.neo4j.graphdb.GraphDatabaseService
import org.rsultan.world.traffic.config.EmbeddedKafkaJunit.{PORT, kafkaRule}
import org.rsultan.world.traffic.config.{EmbeddedKafkaJunit, EmbeddedNeo4JFactory}
import org.rsultan.world.traffic.constant.TestConstants
import org.rsultan.world.traffic.constant.TestConstants.routes
import org.rsultan.world.traffic.serde.RouteSerializer
import org.rsultan.world.traffic.utils.{JobUtils, KafkaUtils}

@RunWith(classOf[BlockJUnit4ClassRunner])
class Kafka2Neo4JTest extends EmbeddedKafkaJunit {


  val args = Seq(
    "--neo4j", "bolt://localhost:7687/db/path",
    "--kafka-brokers", "localhost:" + PORT,
    "--kafka-topic", "routes")


  var graphDb: GraphDatabaseService = null

  @Before
  override def setUp(): Unit = {
    graphDb = EmbeddedNeo4JFactory.start()
  }

  @Test
  def should_insert_data_into_neo4j(): Unit = {
    KafkaUtils.produce(helper = kafkaRule.helper(), topic = "routes", new RouteSerializer, data = routes.map(_.toStationRoute))
    JobUtils.execute(Kafka2Neo4J(args), 20)

    val result = graphDb.execute(
      "MATCH (a1:Airport)-[r]->(a2:Airport) RETURN count(distinct a1) as src, count(distinct a2) as dest, count(r) as rels"
    )

    assertThat(result.hasNext).isTrue
    val map = result.next()

    assertThat(map.get("src").toString.toInt).isEqualTo(3)
    assertThat(map.get("dest").toString.toInt).isEqualTo(3)
    assertThat(map.get("rels").toString.toInt).isEqualTo(6)
  }

  @After
  override def tearDown(): Unit = EmbeddedNeo4JFactory.stop()

}
