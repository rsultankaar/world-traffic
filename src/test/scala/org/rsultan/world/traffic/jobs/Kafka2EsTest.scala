package org.rsultan.world.traffic.jobs

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.runner.RunWith
import org.junit.runners.BlockJUnit4ClassRunner
import org.fest.assertions.api.Assertions.assertThat
import org.junit.{After, Before, Test}
import org.rsultan.world.traffic.config.EmbeddedKafkaJunit.{PORT, kafkaRule}
import pl.allegro.tech.embeddedelasticsearch.EmbeddedElastic
import org.rsultan.world.traffic.config.{EmbeddedElasticFactory, EmbeddedKafkaJunit}
import org.rsultan.world.traffic.constant.TestConstants.airports
import org.rsultan.world.traffic.serde.StationSerializer
import org.rsultan.world.traffic.utils.{JobUtils, KafkaUtils}

@RunWith(classOf[BlockJUnit4ClassRunner])
class Kafka2EsTest extends EmbeddedKafkaJunit {

  private val args = Seq(
    "--elastic-hosts", "http://localhost:9250",
    "--kafka-brokers", "localhost:" + PORT,
    "--kafka-topic", "stations")

  private val mapper = new ObjectMapper
  private var elastic: Option[EmbeddedElastic] = None

  @Before
  override def setUp(): Unit =
    elastic = Some(EmbeddedElasticFactory.start())

  @Test
  def should_insert_data_into_elasticsearch(): Unit = {
    KafkaUtils.produce(helper = kafkaRule.helper(), topic = "stations", new StationSerializer, data = airports.map(_.toStation))
    JobUtils.execute(Kafka2Es(args), timeSec = 20)

    val result = elastic.get.fetchAllDocuments("stations")
    assertThat(result.size).isEqualTo(3)
  }

  @After
  override def tearDown(): Unit =
    EmbeddedElasticFactory.stop()
}
