package org.rsultan.world.traffic.constant

import org.rsultan.world.traffic.model.{Airport, RawRoute}

object TestConstants {

  val LHR = Airport(
    507,
    "London Heathrow Airport", "London", "United Kingdom",
    "LHR", "EGLL", 51.4706f, -0.461941f, 83, 0,
    "E", "Europe/London", "airport", "OurAirports"
  )

  val CDG = Airport(
    1382,
    "Charles de Gaulle International Airport", "Paris", "France", "CDG", "LFPG",
    49.012798f, 2.55f, 392, 1,
    "E", "Europe/Paris", "airport", "OurAirports"
  )

  val JFK = Airport(
    3797, "John F Kennedy International Airport", "New York", "United States", "JFK", "KJFK",
    40.63980103f, -73.77890015f, 13, -5,
    "A", "America/New_York", "airport", "OurAirports"
  )

  def computeRawRoute(src: Airport, dst: Airport): RawRoute = {
    RawRoute(
      src.name, src.city, src.country, src.iata, src.icao, src.lat, src.long, src.altitude, src.utc_timezone,
      dst.name, dst.city, dst.country, dst.iata, dst.icao, dst.lat, dst.long, dst.altitude, dst.utc_timezone
    )
  }

  val airports: Seq[Airport] = Seq(LHR, CDG, JFK)
  val routes: Seq[RawRoute] = Seq(
    computeRawRoute(JFK, CDG),
    computeRawRoute(CDG, JFK),
    computeRawRoute(CDG, LHR),
    computeRawRoute(LHR, CDG),
    computeRawRoute(JFK, LHR),
    computeRawRoute(LHR, JFK)
  )
}