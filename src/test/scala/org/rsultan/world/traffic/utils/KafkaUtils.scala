package org.rsultan.world.traffic.utils

import java.util

import com.github.charithe.kafka.KafkaHelper
import org.fest.assertions.api.Assertions.assertThat
import org.rsultan.world.traffic.serde.KryoSerialisationSchema

import scala.collection.JavaConverters._
import scala.reflect.ClassTag

object KafkaUtils {

  def consume[T: ClassTag](helper: KafkaHelper,
                           topic: String,
                           serializer: KryoSerialisationSchema[T],
                           numMessages: Int): util.List[T] = {
    val consumer = helper.createByteConsumer()
    val value = helper.consume(topic, consumer, numMessages)
    value.get().asScala.map(_.value).map(serializer.deserialize).asJava
  }

  def produce[T](helper: KafkaHelper,
                 topic: String,
                 serializer: KryoSerialisationSchema[T],
                 data: Seq[T]) {
    val producer = helper.createByteProducer()
    val map = data.map(a => serializer.serialize(a) -> serializer.serialize(a)).toMap.asJava
    helper.produce(topic, producer, map)
  }


  def testTopic[T: ClassTag](helper: KafkaHelper, topic: String, numMessages: Int, serializer: KryoSerialisationSchema[T], expected: Seq[T]) {
    val result = consume(helper, topic, serializer, numMessages)
    assertThat(result).hasSize(numMessages)
    numMessages match {
      case 0 => assertThat(result).isEmpty()
      case _ => assertThat(result).containsOnly(expected: _*)
    }
  }
}