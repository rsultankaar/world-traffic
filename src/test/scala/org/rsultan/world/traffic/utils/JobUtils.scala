package org.rsultan.world.traffic.utils

import java.util.concurrent.{Executors, TimeUnit}

import org.rsultan.world.traffic.jobs.Job

object JobUtils {

  private val executor = Executors.newSingleThreadExecutor()

  def execute(job: Job, timeSec:Int) {
    executor.execute(job)
    executor.awaitTermination(timeSec, TimeUnit.SECONDS)
  }
}
