package org.rsultan.world.traffic.serde

import org.rsultan.world.traffic.model.Station
import org.rsultan.world.traffic.serde.StationSerializer

class StationSerializerTest(var counter: Int) extends StationSerializer {

  override def isEndOfStream(nextElement: Station): Boolean = {
    counter += 1; false
  }

}

class RawRouteSerializerTest {}