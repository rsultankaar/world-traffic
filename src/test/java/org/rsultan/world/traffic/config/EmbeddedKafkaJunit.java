package org.rsultan.world.traffic.config;

import com.github.charithe.kafka.EphemeralKafkaBroker;
import com.github.charithe.kafka.KafkaJunitRule;
import junit.framework.TestCase;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.runtime.testutils.MiniClusterResourceConfiguration;
import org.apache.flink.test.util.AbstractTestBase;
import org.apache.flink.test.util.MiniClusterWithClientResource;
import org.junit.ClassRule;
import org.junit.Rule;

import java.util.Properties;

import static com.github.charithe.kafka.KafkaJunitExtensionConfig.ALLOCATE_RANDOM_PORT;
import static org.apache.flink.configuration.ConfigConstants.DEFAULT_PARALLELISM;

public class EmbeddedKafkaJunit extends TestCase {

    protected static final int PORT = 9093;
    private static final Properties props = new Properties();

    static {
        props.put("auto.create.topics.enable", true);
        props.put("advertised.host.name", "localhost");
        props.put("request.timeout.ms", 100000);
        props.put("zookeeper.connection.timeout.ms", 100000);
    }

    @ClassRule
    public static MiniClusterWithClientResource miniClusterResource = new MiniClusterWithClientResource(
            new MiniClusterResourceConfiguration.Builder()
                    .setNumberTaskManagers(1)
                    .setNumberSlotsPerTaskManager(DEFAULT_PARALLELISM)
                    .build());


    @ClassRule
    public static final KafkaJunitRule kafkaRule = new KafkaJunitRule(
            EphemeralKafkaBroker.create(PORT, ALLOCATE_RANDOM_PORT, props)
    );
}
