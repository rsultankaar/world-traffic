package org.rsultan.world.traffic.config;

import pl.allegro.tech.embeddedelasticsearch.EmbeddedElastic;
import java.io.IOException;

import static java.util.concurrent.TimeUnit.MINUTES;
import static pl.allegro.tech.embeddedelasticsearch.PopularProperties.*;

public class EmbeddedElasticFactory {

    private static final String ELASTIC_VERSION = "6.0.0";
    private static final String CLUSTER_NAME_VALUE = "elasticsearch";
    private static final int PORT = 9250;

    private static EmbeddedElastic embeddedElastic = EmbeddedElastic.builder()
            .withElasticVersion(ELASTIC_VERSION)
            .withSetting(HTTP_PORT, PORT)
            .withSetting(CLUSTER_NAME, CLUSTER_NAME_VALUE)
            .withCleanInstallationDirectoryOnStop(true)
            .withStartTimeout(10, MINUTES)
            .build();

    public static EmbeddedElastic start() throws IOException, InterruptedException {
        return embeddedElastic.start();
    }

    public static void stop() {
        embeddedElastic.stop();
    }
}
