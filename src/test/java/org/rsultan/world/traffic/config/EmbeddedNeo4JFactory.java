package org.rsultan.world.traffic.config;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.test.TestGraphDatabaseFactory;

import java.io.File;

import static java.util.Objects.nonNull;

public class EmbeddedNeo4JFactory {


    private static GraphDatabaseService GRAPH_DB;
    public static GraphDatabaseSettings.BoltConnector bolt = GraphDatabaseSettings.boltConnector("0");

    public static GraphDatabaseService start() {
        return (GRAPH_DB = new TestGraphDatabaseFactory()
                .newImpermanentDatabaseBuilder()
                .setConfig(GraphDatabaseSettings.pagecache_memory, "8m")
                .setConfig(bolt.type, "BOLT")
                .setConfig(bolt.enabled, "true")
                .setConfig(bolt.address, "localhost:7687")
                .setConfig(GraphDatabaseSettings.auth_enabled, "false")
                .newGraphDatabase());
    }

    public static void stop() {
        if (nonNull(GRAPH_DB)) {
            GRAPH_DB.shutdown();
        }
    }

}
