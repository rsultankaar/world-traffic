package org.rsultan.world.traffic.jobs

trait Job extends Runnable {
  override def run(): Unit
}
