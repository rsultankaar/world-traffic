package org.rsultan.world.traffic.jobs

import java.util.Properties

import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer
import org.apache.flink.table.api.scala.StreamTableEnvironment
import org.rsultan.world.traffic.conf.Kafka2Neo4JOpts
import org.rsultan.world.traffic.model.{RawRoute, StationRoute}
import org.rsultan.world.traffic.serde.RouteSerializer
import org.rsultan.world.traffic.sink.RouteSink

class Kafka2Neo4J(opts: Kafka2Neo4JOpts, serializer: RouteSerializer = new RouteSerializer) extends Job {

  private val properties = new Properties()
  properties.setProperty("bootstrap.servers", opts.kafkaBrokers())
  properties.setProperty("group.id", getClass.getCanonicalName)
  properties.setProperty("auto.offset.reset", "earliest")

  override def run(): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.enableCheckpointing(5000)

    implicit val tEnv: StreamTableEnvironment = StreamTableEnvironment.create(env)
    implicit val rawRouteType: TypeInformation[RawRoute] = TypeInformation.of(classOf[RawRoute])
    implicit val routeType: TypeInformation[StationRoute] = serializer.getProducedType

    val routeStreams = env
      .addSource(new FlinkKafkaConsumer[StationRoute](opts.kafkaTopic(), serializer, properties))

    routeStreams
      .addSink(new RouteSink(opts.neo4j()))

    env.execute("Routes and to Neo4J")
  }
}

object Kafka2Neo4J {
  def apply(opts: Seq[String]): Kafka2Neo4J = new Kafka2Neo4J(new Kafka2Neo4JOpts(opts))
}