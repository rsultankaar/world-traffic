package org.rsultan.world.traffic.jobs

import java.util.Properties

import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer
import org.apache.flink.table.api.scala.StreamTableEnvironment
import org.rsultan.world.traffic.builder.StationSinkBuilder
import org.rsultan.world.traffic.conf.Kafka2EsOpts
import org.rsultan.world.traffic.model.Station
import org.rsultan.world.traffic.serde.StationSerializer

class Kafka2Es(opts: Kafka2EsOpts, serializer: StationSerializer) extends Job {

  override def run(): Unit = {
    val properties = new Properties()
    properties.setProperty("bootstrap.servers", opts.kafkaBrokers())
    properties.setProperty("group.id", getClass.getCanonicalName)
    properties.setProperty("auto.offset.reset", "earliest")

    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.enableCheckpointing(5000)

    implicit val tEnv: StreamTableEnvironment = StreamTableEnvironment.create(env)
    implicit val stationType: TypeInformation[Station] = serializer.getProducedType

    val airportStream = env
      .addSource(new FlinkKafkaConsumer[Station](opts.kafkaTopic(), serializer, properties))

    val esSinkBuilder = StationSinkBuilder(opts.elasticHosts(), "stations", "station")
    esSinkBuilder.setBulkFlushMaxActions(1)

    airportStream.addSink(esSinkBuilder.build)
    env.execute("Stations to Es")
  }
}

object Kafka2Es {
  def apply(opts: Seq[String], serializer: StationSerializer = new StationSerializer): Kafka2Es =
    new Kafka2Es(new Kafka2EsOpts(opts), serializer)
}