package org.rsultan.world.traffic.jobs

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.table.api.scala.StreamTableEnvironment
import org.rsultan.world.traffic.model._
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer
import org.apache.flink.table.api.Table
import org.apache.flink.table.api.scala._
import org.rsultan.world.traffic.conf.File2KafkaOpts
import org.rsultan.world.traffic.serde.{RouteSerializer, StationSerializer}

class Airport2Kafka(opts: File2KafkaOpts) extends Job {

  override def run(): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    implicit val tEnv: StreamTableEnvironment = StreamTableEnvironment.create(env)
    val tAirport: Table = Airports(path = opts.airports()) getTable
    val tRoutes = AiportRoutes(path = opts.routes()) getJoinTable tAirport

    val stationsProducer = new FlinkKafkaProducer[Station](opts.kafkaBrokers(),
      opts.airportTopic(),
      new StationSerializer)
    stationsProducer.setWriteTimestampToKafka(true)

    val routeProducer = new FlinkKafkaProducer[StationRoute](opts.kafkaBrokers(),
      opts.routesTopic(),
      new RouteSerializer)
    stationsProducer.setWriteTimestampToKafka(true)

    tAirport.toAppendStream[Airport]
      .map(_.toStation)
      .addSink(stationsProducer)

    tRoutes
      .toAppendStream[RawRoute]
      .map(_.toStationRoute)
      .addSink(routeProducer)
    env.execute("Station to Sinks")
  }
}

object Airport2Kafka {
  def apply(opts: Seq[String]): Airport2Kafka = new Airport2Kafka(new File2KafkaOpts(opts))
}
