package org.rsultan.world.traffic.conf

import org.rogach.scallop.{ScallopConf, ScallopOption}

class JobOpts(arguments: Seq[String]) extends ScallopConf(arguments){
  val job: ScallopOption[String] = opt[String](required = true)
  verify()
}

class File2KafkaOpts(arguments: Seq[String]) extends ScallopConf(arguments) {
  val airports: ScallopOption[String] = opt[String](required = true)
  val routes: ScallopOption[String] = opt[String](required = true)
  val kafkaBrokers: ScallopOption[String] = opt[String](default = Some("localhost:9099"))
  val airportTopic: ScallopOption[String] = opt[String](required = true)
  val routesTopic: ScallopOption[String] = opt[String](required = true)
  verify()
}

class Kafka2EsOpts(arguments: Seq[String]) extends ScallopConf(arguments) {
  val kafkaBrokers: ScallopOption[String] = opt[String](default = Some("localhost:9099"))
  val kafkaTopic: ScallopOption[String] = opt[String](required = true)
  val elasticHosts: ScallopOption[String] = opt[String](default = Some("localhost:9200"))
  verify()
}

class Kafka2Neo4JOpts(arguments: Seq[String]) extends ScallopConf(arguments) {
  val kafkaBrokers: ScallopOption[String] = opt[String](default = Some("localhost:9099"))
  val kafkaTopic: ScallopOption[String] = opt[String](required = true)
  val neo4j: ScallopOption[String] = opt[String](required = true)
  verify()
}
