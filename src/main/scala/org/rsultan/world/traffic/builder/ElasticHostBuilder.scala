package org.rsultan.world.traffic.builder

import org.apache.http.HttpHost
import scala.util.matching.Regex
import collection.JavaConverters._

class ElasticHostBuilder(hosts: String) {

  val SCHEME_HOST_PORT: Regex = "(https?)\\://(.+):([0-9]+)".r
  val SCHEME_HOST: Regex = "(https?)\\://(.+)".r
  val HOST_PORT: Regex = "(.+):([0-9]+)".r

  def build: java.util.List[HttpHost] = {
    hosts.split(",")
      .map {
        case SCHEME_HOST_PORT(scheme: String, host: String, port) => (scheme, host, port.toInt)
        case SCHEME_HOST(scheme: String, host: String) => (scheme, host, 9200)
        case HOST_PORT(host: String, port: String) => ("http", host, port.toInt)
        case _ => ("http", "localhost", 9200)
      }
      .distinct
      .map(n => new HttpHost(n._2, n._3, n._1))
      .toSeq.asJava
  }

}

object ElasticHostBuilder {

  def apply(hosts: String): ElasticHostBuilder = new ElasticHostBuilder(hosts)
}
