package org.rsultan.world.traffic.builder

import org.apache.flink.api.common.functions.RuntimeContext
import org.apache.flink.streaming.connectors.elasticsearch.{ElasticsearchSinkFunction, RequestIndexer}
import org.apache.flink.streaming.connectors.elasticsearch6.ElasticsearchSink
import org.elasticsearch.action.update.UpdateRequest
import org.elasticsearch.common.xcontent.XContentType
import org.rsultan.world.traffic.model.Station

case class StationSinkBuilder(hosts: String, index: String, docType: String)
  extends ElasticsearchSink.Builder[Station](ElasticHostBuilder(hosts).build, AirportFn(index, docType))

case class AirportFn(index: String, docType: String) extends ElasticsearchSinkFunction[Station] {

  def createIndexRequest(element: Station): UpdateRequest = {
    new UpdateRequest()
      .index(index)
      .`type`(docType)
      .id(element.map.get("id").toString)
      .doc(element.map, XContentType.JSON)
      .upsert(element.map, XContentType.JSON)
  }

  override def process(element: Station, ctx: RuntimeContext, indexer: RequestIndexer): Unit =
    indexer.add(createIndexRequest(element))
}



