package org.rsultan.world.traffic

import org.rsultan.world.traffic.conf.JobOpts
import org.rsultan.world.traffic.jobs.{Airport2Kafka, Kafka2Es, Kafka2Neo4J}

object Main {

  def main(args: Array[String]) {
    //TODO: Better Argument handling.
    val jobArg = args.zipWithIndex.filter(_._2 < 2).map(_._1)
    val conf = new JobOpts(jobArg)
    val opts = args.zipWithIndex.filter(_._2 > 1).map(_._1)

    val job = conf.job().toLowerCase match {
      case "file2kafka" => Airport2Kafka(opts)
      case "kafka2es" => Kafka2Es(opts)
      case "kafka2neo4j" => Kafka2Neo4J(opts)
      case _ => throw new IllegalArgumentException(s"Unknown job ${conf.job()}")
    }

    job.run()
  }
}