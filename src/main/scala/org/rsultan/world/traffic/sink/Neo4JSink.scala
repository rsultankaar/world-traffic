package org.rsultan.world.traffic.sink

import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction
import org.neo4j.driver.{Driver, GraphDatabase, Session}

abstract class Neo4JSink[T](url: String) extends RichSinkFunction[T] {

  @transient private lazy val driver: Driver = GraphDatabase.driver(url)
  protected var session: Option[Session] = None
  protected var query: Option[String] = None

  override def open(parameters: Configuration): Unit = {
    super.open(parameters)
    session = Some(getSession)
  }

  def getSession: Session = session match {
    case Some(s:Session) => s
    case _ =>
      session = Some(driver.session())
      getSession
  }

  override def close(): Unit = {
    super.close()
    getSession.close()
  }

  def setQuery(queryString: String): Unit = query = Some(queryString)

  protected def getQuery: String =
    query match {
      case Some(q: String) => q
      case _ => throw new IllegalArgumentException("Query must be set.")
    }
}