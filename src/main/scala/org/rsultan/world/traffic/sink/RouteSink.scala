package org.rsultan.world.traffic.sink

import org.apache.flink.streaming.api.functions.sink.SinkFunction
import org.neo4j.driver.Values.parameters
import org.neo4j.driver.{Transaction, TransactionWork}
import org.rsultan.world.traffic.model.Airports._
import org.rsultan.world.traffic.model.StationRoute
import org.slf4j.{Logger, LoggerFactory}

import scala.util.{Failure, Try}

class RouteSink(url: String) extends Neo4JSink[StationRoute](url) {

  val LOG: Logger = LoggerFactory.getLogger(classOf[RouteSink])

  setQuery(
    "MERGE (srcCountry:Country{name: $srcCountry}) " +
      "MERGE (srcCity:City {name:$srcCity}) " +
      "MERGE (a1:Airport {name:$srcName, IATA:$srcIata , ICAO:$srcIcao, lat: $srcLat, long:$srcLong, alt:$srcAlt, tz: $srcTz}) " +
      "MERGE (a2:Airport {name:$dstName, IATA:$dstIata, ICAO:$dstIcao, lat:$dstLat, long:$dstLong, alt:$dstAlt, tz:$dstTz}) " +
      "MERGE (dstCity:City {name:$dstCity}) " +
      "MERGE (dstCountry:Country{name: $dstCountry}) " +
      "MERGE (srcCountry)<-[r1:HAS_COUNTRY]-(srcCity) " +
      "MERGE (srcCity)<-[r2:HAS_CITY]-(a1) " +
      "MERGE (dstCountry)<-[r3:HAS_COUNTRY]-(dstCity)  " +
      "MERGE (dstCity)<-[r4:HAS_CITY]-(a2) " +
      "MERGE (a1)-[d:HAS_DESTINATION]->(a2) " +
      "MERGE (a1)<-[d2:HAS_DESTINATION]-(a2)"
  )

  private def anyRef(x: Any): AnyRef = x match {
    case int: Int => int.asInstanceOf[java.lang.Integer]
    case float: Float => float.asInstanceOf[java.lang.Float]
    case double: Double => double.asInstanceOf[java.lang.Double]
    case _ => x.toString
  }

  override def invoke(value: StationRoute, context: SinkFunction.Context[_]): Unit = {
    val src = value.src.map
    val dst = value.dst.map
    val tryReq: Try[Int] = Try(
      {
        val params = parameters(
          "srcName", anyRef(src.get(name)), "srcCity", anyRef(src.get(city)), "srcCountry", anyRef(src.get(country)),
          "srcIata", anyRef(src.get(iata)), "srcIcao", anyRef(src.get(icao)), "srcLat", anyRef(src.get(lat)),
          "srcLong", anyRef(src.get(long)), "srcAlt", anyRef(src.get(alt)), "srcTz", anyRef(src.get(utc_tz)),
          "dstName", anyRef(dst.get(name)), "dstCity", anyRef(dst.get(city)), "dstCountry", anyRef(dst.get(country)),
          "dstIata", anyRef(dst.get(iata)), "dstIcao", anyRef(dst.get(icao)), "dstLat", anyRef(dst.get(lat)),
          "dstLong", anyRef(dst.get(long)), "dstAlt", anyRef(dst.get(alt)), "dstTz", anyRef(dst.get(utc_tz))
        ).asMap()
        getSession.writeTransaction(new TransactionWork[Int] {
          override def execute(tx: Transaction): Int = {
            tx.run(getQuery, params)
            1
          }
        })
      }
    )
    tryReq match {
      case error: Failure[Int] => throw error.failed.get
      case _ => LOG.info("Graph for value {} uploaded successfully", value)
    }
  }
}
