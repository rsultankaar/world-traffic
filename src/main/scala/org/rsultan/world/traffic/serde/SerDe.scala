package org.rsultan.world.traffic.serde

import org.apache.flink.api.common.typeinfo.TypeInformation
import org.rsultan.world.traffic.model.{RawRoute, Station, StationRoute}

class StationSerializer extends KryoSerialisationSchema[Station](classOf[Station]){
  override def isEndOfStream(nextElement: Station): Boolean = false
  override def getProducedType: TypeInformation[Station] = TypeInformation.of(classOf[Station])
}

class RouteSerializer extends KryoSerialisationSchema [StationRoute](classOf[StationRoute]){
  override def isEndOfStream(nextElement: StationRoute): Boolean = false
  override def getProducedType: TypeInformation[StationRoute] = TypeInformation.of(classOf[StationRoute])
}

