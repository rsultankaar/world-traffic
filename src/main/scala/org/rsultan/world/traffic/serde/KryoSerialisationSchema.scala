package org.rsultan.world.traffic.serde

import java.io.{ByteArrayInputStream, ByteArrayOutputStream}

import com.esotericsoftware.kryo.Kryo
import com.esotericsoftware.kryo.io.{Input, Output}
import org.apache.flink.api.common.serialization.{DeserializationSchema, SerializationSchema}

abstract class KryoSerialisationSchema[T](clazz:Class[T])  extends DeserializationSchema[T] with SerializationSchema[T]{

  override def serialize(elt: T): Array[Byte] = {
    try
        if (elt == null) null
        else {
          val kryo: Kryo = getKryo
          val stream = new ByteArrayOutputStream
          val output = new Output(stream)
          kryo.writeObject(output, elt)
          output.close()
          stream.toByteArray
        }
    catch {
      case e: Throwable => throw e
    }
  }

  override def deserialize(data: Array[Byte]): T = {
    val kryo: Kryo = getKryo
    val input = new Input(new ByteArrayInputStream(data))
    val elt = kryo.readObject(input, clazz)
    input.close()
    elt
  }

  private def getKryo = {
    val kryo = new Kryo()
    kryo.setRegistrationRequired(false)
    kryo.setClassLoader(Thread.currentThread().getContextClassLoader)
    kryo
  }
}
