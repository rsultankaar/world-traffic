package org.rsultan.world.traffic.model

import java.util

import org.apache.flink.api.scala.typeutils.Types
import org.apache.flink.table.api.Table
import org.apache.flink.table.sources.CsvTableSource
import org.elasticsearch.common.geo.GeoPoint
import org.rsultan.world.traffic.model.Airports._

import scala.Float.PositiveInfinity
import scala.Int.MinValue


case class Airport(id: Int, name: String, city: String, country: String, iata: String, icao: String,
                   lat: Float, long: Float, altitude: Float, utc_timezone: Float,
                   dst: String, olson_timezone: String, `type`: String, data_source: String) {

  def this() = this(MinValue, "", "", "", "", "", PositiveInfinity, PositiveInfinity, PositiveInfinity, PositiveInfinity, "", "", "", "")

  def toLight: AirportLight =
    AirportLight(name, city, country, iata, icao, lat, long, altitude, utc_timezone)

  def toStation: Station = {
    val map = new util.HashMap[String, Any]()
    map.put("id", id)
    if (!name.isEmpty) map.put(Airports.name, name)
    if (!city.isEmpty) map.put(Airports.city, city)
    if (!country.isEmpty) map.put(Airports.country, country)
    if (!iata.isEmpty) map.put(Airports.iata, iata)
    if (!icao.isEmpty) map.put(Airports.icao, icao)

    if (!PositiveInfinity.equals(lat) && !PositiveInfinity.equals(long))
      map.put("location", new GeoPoint(lat, long))

    if (!PositiveInfinity.equals(altitude)) map.put(Airports.alt, altitude)
    if (!PositiveInfinity.equals(utc_timezone)) map.put(Airports.utc_tz, utc_timezone)
    if (!dst.isEmpty) map.put(Airports.dst, dst)
    if (!olson_timezone.isEmpty) map.put(Airports.olson_tz, olson_timezone)
    if (!olson_timezone.isEmpty) map.put(Airports.data_source, data_source)

    map.put("type", classOf[Airport].getName.toLowerCase)
    Station(map)
  }
}

case class AirportLight(name: String, city: String, country: String, iata: String, icao: String,
                        lat: java.lang.Float, long: java.lang.Float, alt: java.lang.Float,
                        utc_timezone: java.lang.Float) {
  def toStation: Station = {
    val map = new util.HashMap[String, Any]()
    if (!name.isEmpty) map.put(Airports.name, name)
    if (!city.isEmpty) map.put(Airports.city, city)
    if (!country.isEmpty) map.put(Airports.country, country)
    if (!iata.isEmpty) map.put(Airports.iata, iata)
    if (!icao.isEmpty) map.put(Airports.icao, icao)
    if (!PositiveInfinity.equals(lat) && !PositiveInfinity.equals(long)) {
      map.put(Airports.lat, lat)
      map.put(Airports.long, long)
    }
    if (!PositiveInfinity.equals(alt)) map.put(Airports.alt, alt)
    if (!PositiveInfinity.equals(utc_timezone)) map.put(Airports.utc_tz, utc_timezone)

    map.put("type", classOf[Airport].getSimpleName)

    Station(map)
  }
}

class Airports(tableName: String = "airports", path: String)(implicit tEnv: org.apache.flink.table.api.TableEnvironment) {

  def getTable: Table = {
    val airports = CsvTableSource.builder().path(path)
      .fieldDelimiter(",")
      .quoteCharacter('"')
      .field("id", Types.INT)
      .field(name, Types.STRING)
      .field(city, Types.STRING)
      .field(country, Types.STRING)
      .field(iata, Types.STRING)
      .field(icao, Types.STRING)
      .field(lat, Types.FLOAT)
      .field(long, Types.FLOAT)
      .field(alt, Types.FLOAT)
      .field(utc_tz, Types.FLOAT)
      .field(dst, Types.STRING)
      .field(olson_tz, Types.STRING)
      .field(`type`, Types.STRING)
      .field(data_source, Types.STRING)
      .build

    tEnv.registerTableSource(tableName, airports)
    tEnv.sqlQuery(
      s"""SELECT id,
         | COALESCE($name, '') as $name,
         | COALESCE($city, '') as $city,
         | COALESCE($country, '') as $country,
         | COALESCE($iata, '') as $iata,
         | COALESCE($icao, '') as $icao,
         | $lat, $long, $alt,
         | COALESCE($utc_tz, 0) as $utc_tz,
         | COALESCE($dst, '') as $dst,
         | COALESCE($olson_tz, '') as $olson_tz,
         | COALESCE(${`type`}, '') as ${`type`},
         | COALESCE($data_source, '') as $data_source
         | FROM $tableName""".stripMargin
    )
  }
}

object Airports {

  val name = "name"
  val city = "city"
  val country = "country"
  val iata = "IATA"
  val icao = "ICAO"
  val lat = "lat"
  val long = "long"
  val alt = "alt"
  val utc_tz = "utc_timezone"
  val dst = "dst"
  val olson_tz = "olson_timezone"
  val `type` = "type"
  val data_source = "data_source"

  private val defaultTableName = "airports"

  def apply(tableName: String = defaultTableName, path: String)
           (implicit env: org.apache.flink.table.api.TableEnvironment): Airports =
    new Airports(tableName, path)
}
