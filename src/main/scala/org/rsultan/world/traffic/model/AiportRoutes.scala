package org.rsultan.world.traffic.model

import java.util

import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.scala.typeutils.Types
import org.apache.flink.table.api.Table
import org.apache.flink.table.api.scala._
import org.apache.flink.table.sources.CsvTableSource
import org.rsultan.world.traffic.model.AiportRoutes.{defaultTableName, dest, source}

import scala.Float.PositiveInfinity

case class RawRoute(
                     srcName: String, srcCity: String, srcCountry: String, srcIata: String, srcIcao: String,
                     srcLat: Float, srcLong: Float, srcAlt: Float, srcTz: Float,
                     destName: String, destCity: String, destCountry: String, destIata: String, destIcao: String,
                     destLat: Float, destLong: Float, destAlt: Float, destTz: Float
                   ) {

  def this() = this(
    "", "", "", "", "", PositiveInfinity, PositiveInfinity, PositiveInfinity, PositiveInfinity,
    "", "", "", "", "", PositiveInfinity, PositiveInfinity, PositiveInfinity, PositiveInfinity
  )

  def toStationRoute: StationRoute = StationRoute(
    AirportLight(srcName, srcCity, srcCountry, srcIata, srcIcao, srcLat, srcLong, srcAlt, srcTz).toStation,
    AirportLight(destName, destCity, destCountry, destIata, destIcao, destLat, destLong, destAlt, destTz).toStation
  )
}

class AiportRoutes(tableName: String = defaultTableName, path: String)(implicit tEnv: org.apache.flink.table.api.TableEnvironment) {

  def getTable: Table = {
    val routes = CsvTableSource.builder().path(path)
      .fieldDelimiter(",")
      .quoteCharacter('"')
      .field(source, Types.STRING)
      .field(dest, Types.STRING)
      .build

    tEnv.registerTableSource(tableName, routes)
    tEnv.sqlQuery(s"SELECT ${source}, ${dest} FROM $tableName")
  }

  def getJoinTable(tStations: Table) = {
    implicit val typeInfo: TypeInformation[RawRoute] = TypeInformation.of(classOf[RawRoute])
    getTable.join(tStations).where("src === IATA")
      .select(
        'name.as('srcName), 'city.as('srcCity), 'country.as('srcCountry), 'IATA.as('srcIata),
        'ICAO.as('srcIcao), 'lat.as('srcLat), 'long.as('srcLong), 'alt.as('srcAlt),
        'utc_timezone.as('srcTz), 'dest
      )
      .join(tStations).where("dest === IATA")
      .select(
        'srcName, 'srcCity, 'srcCountry, 'srcIata, 'srcIcao, 'srcLat, 'srcLong, 'srcAlt, 'srcTz,
        'name.as('destName), 'city.as('destCity), 'country.as('destCountry), 'IATA.as('destIata),
        'ICAO.as('destIcao), 'lat.as('destLat), 'long.as('destLong), 'alt.as('destAlt),
        'utc_timezone.as('destTz)
      )
  }
}

object AiportRoutes {

  private val source = "src"
  private val dest = "dest"
  private val defaultTableName = "routes"

  def apply(tableName: String = defaultTableName, path: String)
           (implicit env: org.apache.flink.table.api.TableEnvironment): AiportRoutes =
    new AiportRoutes(tableName, path)
}
