package org.rsultan.world.traffic.model

import java.util

case class Station(map: util.Map[String, Any]) {

  def this() {
    this(new util.HashMap[String, Any]())
  }
}

case class StationRoute(src: Station, dst: Station) {

  def this() {
    this(new Station(), new Station())
  }
}