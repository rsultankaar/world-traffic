
# World traffic

## 1. How to 

Go to the root and 

```
$ mvn clean install
```

### Start all of the services

```
 $ docker-compose up
```
### Run file2kafka

```
 $ docker run --network worldtraffic_default \
--rm -v "/opt/data:/opt/data" \
-d \
--name file2kafka \
--entrypoint './bin/flink' flink-world run -p 2 -m jobmanager:8081 ./jars/world-traffic.jar \
--job File2Kafka --airports /opt/data/airports.txt --routes /opt/data/routes.txt \
--kafka-brokers kafka:9092 \
--airport-topic stations --routes-topic routes
```

### Run kafka2es

```
 $ docker run --network worldtraffic_default \
--rm -v "/opt/data:/opt/data" \
-d \
--name file2kafka \
--entrypoint './bin/flink' flink-world run -p 2 -m jobmanager:8081 ./jars/world-traffic.jar \
--job Kafka2Es --elastic-hosts http://elastic01:9200,http://elastic02:9200 \
--kafka-brokers kafka:9092 \
--kafka-topic stations
```

### Run kafka2neo4j


```
 $ docker run --network worldtraffic_default \ 
--rm -v "/opt/data:/opt/data" \
-d \
--name kafka2neo4j \
--entrypoint './bin/flink' flink-world run -p 2 -m jobmanager:8081 ./jars/world-traffic.jar \
--job File2Kafka --neo4j bolt://neo4j:7687 \
--kafka-brokers kafka:9092 \
--kafka-topic routes
```