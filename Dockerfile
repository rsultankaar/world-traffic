FROM flink:1.9.0-scala_2.12

COPY target/world-traffic.jar ${FLINK_HOME}/jars/

EXPOSE 6123 8081
CMD ["help"]
